#pragma once
#include <queue>
#include <mutex>
#include <condition_variable>

namespace ribomation::concurrent {

    template<typename MessageType>
    class MessageQueueUnbounded {
        std::queue<MessageType> inbox{};
        std::mutex exclusive{};
        std::condition_variable not_empty{};
    public:
        MessageQueueUnbounded() = default;
        MessageQueueUnbounded(MessageQueueUnbounded const&) = delete;
        auto operator =(MessageQueueUnbounded const&) -> MessageQueueUnbounded = delete;

        void put(MessageType msg) {
            {
                auto guard = std::lock_guard<std::mutex>{exclusive};
                inbox.push(msg);
            }
            not_empty.notify_all();
        }

        MessageType get() {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_empty.wait(guard, [this] { return not inbox.empty(); });
            auto msg = inbox.front();
            inbox.pop();
            return msg;
        }
    };

}


