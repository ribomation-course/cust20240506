#pragma once

#include <thread>
#include <atomic>
#include <functional>
#include <vector>
#include "message-queue-unbounded.hxx"

namespace ribomation::concurrent {

    class ThreadPool {
        using Task = std::function<void()>;
        std::atomic_bool done{};
        MessageQueueUnbounded<Task> tasks{};
        std::vector<std::jthread> threads{};

        void worker_loop() {
            do {
                auto task = tasks.get();
                if (done) break;
                task();
            } while (!done);
        }
        void create(unsigned num_workers) {
            try {
                for (auto k = 0U; k < num_workers; ++k)
                    threads.emplace_back(&ThreadPool::worker_loop, this);
            } catch (...) { done = true; throw; }
        }
        void shutdown() {
            done = true;
            for (auto k = 0UL; k < threads.size(); ++k) tasks.put([] { return; });
            for (auto&& t: threads) if (t.joinable()) t.join();
        }
    public:
        ThreadPool() : ThreadPool{std::thread::hardware_concurrency()} {}
        explicit ThreadPool(unsigned num_workers) { create(num_workers); }
        ~ThreadPool() { shutdown(); }
        template<typename FunctionType>
        void submit(FunctionType task) { tasks.put(Task{task}); }
    };

}

