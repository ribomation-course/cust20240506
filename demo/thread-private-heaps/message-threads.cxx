#include <iostream>
#include <syncstream>
#include <memory>
#include <memory_resource>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <latch>
#include <string>
#include <array>
#include <random>
#include <vector>

using byte = unsigned char;
using std::cout;
using std::osyncstream;
namespace m = std::pmr;
namespace r = std::ranges;
namespace cr = std::chrono;


template<typename PayloadType, unsigned CAPACITY>
struct MessageQueue {
    std::array<PayloadType, CAPACITY> queue{};
    unsigned size = 0, putIx = 0, getIx = 0;
    std::mutex exclusive{};
    std::condition_variable not_empty{};
    std::condition_variable not_full{};

    bool empty() const { return size == 0; }

    bool full() const { return size == CAPACITY; }

    void put(PayloadType x) {
        auto g = std::unique_lock{exclusive};
        not_full.wait(g, [this] { return not full(); });
        queue[putIx] = x;
        putIx = (putIx + 1) % CAPACITY;
        ++size;
        not_empty.notify_all();
    }

    auto get() -> PayloadType {
        auto g = std::unique_lock{exclusive};
        not_empty.wait(g, [this] { return not empty(); });
        PayloadType x = queue[getIx];
        getIx = (getIx + 1) % CAPACITY;
        --size;
        not_full.notify_all();
        return x;
    }
};

template<typename MessageType>
struct ReceiverThread {
    m::synchronized_pool_resource* myMsgHeap = nullptr;
    MessageQueue<MessageType*, 32> inbox{};
    unsigned const id;
    std::latch ready{1};
    explicit ReceiverThread(const unsigned int id) : id(id) {}

    void run() {
        byte storage[100'000]{};
        auto buff = m::monotonic_buffer_resource{std::data(storage),
                                                 std::size(storage),
                                                 m::null_memory_resource()};
        auto pool = m::synchronized_pool_resource{&buff};
        this->myMsgHeap = &pool;
        application();
    }

    void application() {
        ready.count_down(1);
        osyncstream{cout} << "thr-" << id << ": enter\n";
        for (auto msg = inbox.get(); true; msg = inbox.get()) {
            auto m = *msg;
            std::destroy_at(msg);
            myMsgHeap->deallocate(msg, sizeof(MessageType));

            osyncstream{cout} << "thr-" << id << ": " << m << "\n";
            if (m.done) break;
            std::this_thread::sleep_for(cr::milliseconds(10));
        }
        osyncstream{cout} << "thr-" << id << ": exit\n";
    }

    void send(MessageType msg) {
        auto mem = myMsgHeap->allocate(sizeof(MessageType));
        auto obj = new(mem) MessageType{msg};
        inbox.put(obj);
    }

    template<typename ... Args>
    void send(Args ... args) {
        auto mem = myMsgHeap->allocate(sizeof(MessageType));
        auto obj = new(mem) MessageType{args...};
        inbox.put(obj);
    }

};


struct Message {
    int number{};
    std::string text{};
    bool done{};
    Message() : done{true} {}
    Message(int number, std::string text, bool done = false)
            : number(number), text(std::move(text)), done(done) {}
};
auto operator<<(std::ostream& os, Message const& msg) -> std::ostream& {
    if (msg.done) return os << "Msg{}";
    return os << "MSG{" << msg.number << ", " << msg.text << "}";
}

int main() {
    auto const N = 1000;
    {
        auto recv1 = ReceiverThread<Message>{1};
        auto recv2 = ReceiverThread<Message>{2};
        auto recv3 = ReceiverThread<Message>{3};
        auto thr1 = std::jthread{&ReceiverThread<Message>::run, &recv1};
        auto thr2 = std::jthread{&ReceiverThread<Message>::run, &recv2};
        auto thr3 = std::jthread{&ReceiverThread<Message>::run, &recv3};
        recv1.ready.wait();
        recv2.ready.wait();
        recv3.ready.wait();

        auto R = std::random_device{};
        auto mk = [&R](auto k) -> std::string {
            auto CHAR = std::uniform_int_distribution<char>{'A', 'Z'};
            auto const L = 5 + 3 * k % 31;
            auto str = std::string{};
            for (auto j = 0; j < L; ++j) str.push_back(CHAR(R));
            return str;
        };
        for (auto k = 1; k <= N; ++k) {
            if (k % 3 == 0) recv1.send(k, mk(k));
            if (k % 3 == 1) recv2.send(k, mk(k));
            if (k % 3 == 2) recv3.send(k, mk(k));
        }
        recv1.send(Message{});
        recv2.send(Message{});
        recv3.send(Message{});
    }
    cout << "[main] exit\n";
}


