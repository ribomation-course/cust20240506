#include <iostream>
#include <syncstream>
#include <memory>
#include <memory_resource>
#include <thread>
#include <string>
#include <random>
#include <chrono>
#include <algorithm>
#include <vector>

using byte = unsigned char;
using std::cout;
using std::osyncstream;
namespace m = std::pmr;
namespace r = std::ranges;
namespace cr = std::chrono;

struct spy_unsynchronized_pool_resource : m::unsynchronized_pool_resource {
    using super = m::unsynchronized_pool_resource;
    unsigned long cur_used = 0;
    unsigned long max_used = 0;
    spy_unsynchronized_pool_resource(m::monotonic_buffer_resource* upstream_) : super{upstream_} {}
protected:
    void* do_allocate(size_t num_bytes, size_t __alignment) override {
        cur_used += num_bytes;
        max_used += num_bytes;
        return super::do_allocate(num_bytes, __alignment);
    }
    void do_deallocate(void* ptr, size_t num_bytes, size_t __alignment) override {
        cur_used -= num_bytes;
        super::do_deallocate(ptr, num_bytes, __alignment);
    }
};

struct ThreadWithHeap {
    m::unsynchronized_pool_resource* myHeap = nullptr;
    unsigned const id;
    unsigned const numPrints;
    mutable std::default_random_engine R{};

    ThreadWithHeap(unsigned id_, unsigned numPrints_)
            : id{id_}, numPrints{numPrints_} {
        R.seed(cr::system_clock::now().time_since_epoch().count());
    }

    void operator()() {
        byte storage[10'000]{};
        auto buff = m::monotonic_buffer_resource{std::data(storage),
                                                 std::size(storage),
                                                 m::null_memory_resource()};
        auto pool = spy_unsynchronized_pool_resource{&buff};
        this->myHeap = &pool;
        application();
        osyncstream{cout} << "thr-" << id << ": "
                          << "max=" << pool.max_used << " bytes, "
                          << "cur=" << pool.cur_used << " bytes\n";
    }

    void application() {
        osyncstream{cout} << "thr-" << id << ": started\n";
        for (auto k = 1U; k <= numPrints; ++k) {
            osyncstream{cout} << "thr-" << id << ": " << mkStr() << "\n";
            std::this_thread::sleep_for(cr::milliseconds(10));
        }
    }

    [[nodiscard]] auto mkStr() const -> m::string {
        auto LENGTH = std::uniform_int_distribution<unsigned>{10, 100};
        auto CHAR = std::uniform_int_distribution<char>{'A', 'Z'};
        auto L = LENGTH(R);
        auto str = m::string{myHeap};
        str.reserve(L);
        while (L-- > 0) str.push_back(CHAR(R));
        return str;
    }

    void* operator new(size_t) { throw std::bad_alloc{}; }
    void* operator new[](size_t) { throw std::bad_alloc{}; }
};

int main() {
    auto const T = 10U;
    auto const N = 100U;
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) {
            threads.emplace_back(ThreadWithHeap{id, N});
        }
    }
    cout << "[main] exit\n";
}
