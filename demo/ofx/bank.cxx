#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <functional>
#include <vector>
#include "OMOSFactory.hxx"

int exclusive(ofx::OMOSMutex* mx, std::function<int()> stmts) {
    mx->lock();
    auto result = stmts();
    mx->unlock();
    return result;
}

class Account {
    mutable std::unique_ptr<ofx::OMOSMutex> mutex;
    int balance = 0;
public:
    Account() : mutex(ofx::OMOSFactory::instance()->createOMOSMutex()) {}
    auto get() const {
        return exclusive(mutex.get(), [this]() {
            return balance;
        });
    }
    auto update(int amount) {
        return exclusive(mutex.get(), [this, amount]() {
            balance += amount;
            return balance;
        });
    }
};

struct Params {
    unsigned id;
    Account* acc;
    unsigned num_updates;
    Params(unsigned int id, Account* acc, unsigned int numUpdates)
            : id(id), acc(acc), num_updates(numUpdates) {}
};

void UpdaterBodyFn(void* params) {
    auto p = std::unique_ptr<Params>(reinterpret_cast<Params*>(params));
    {
        auto buf = std::ostringstream{};
        buf << "[updater-" << std::to_string(p->id) << "] started\n";
        std::cout << buf.str();
    }
    for (auto k = 0U; k < p->num_updates; ++k) {
        p->acc->update(+100);
    }
    for (auto k = 0U; k < p->num_updates; ++k) {
        p->acc->update(-100);
    }
    {
        auto buf = std::ostringstream{};
        buf << "[updater-" << std::to_string(p->id) << "] done\n";
        std::cout << buf.str();
    }
}


int main(int argc, char** argv) {
    auto U = 5U;
    auto N = 1'000'000U;

    std::cout << "[main] #updaters = " << U << "\n";
    std::cout << "[main] #updates  = " << N << "\n";
    auto ofx         = ofx::OMOSFactory::instance();
    auto the_account = Account{};
    auto updaters    = std::vector<std::unique_ptr<ofx::OMOSThread>>{};
    updaters.reserve(U);
    for (auto id = 1U; id <= U; ++id) {
        auto p = new Params{id, &the_account, N};
        auto t = ofx->createOMOSThread(&UpdaterBodyFn, p);
        updaters.emplace_back(t);
    }
    for (auto&& u: updaters) u->start();
    for (auto&& u: updaters) ofx->waitOnThread(u->getOsHandle(), 0);
    std::cout << "[main] final balance = " << the_account.get() << "\n";
}
