#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include "OMOSFactory.hxx"

struct Params {
    unsigned thread_id;
    unsigned num_turns;
    unsigned sleep_secs;
    Params(unsigned int threadId, unsigned int numTurns, unsigned int sleepSecs)
            : thread_id(threadId),
              num_turns(numTurns),
              sleep_secs(sleepSecs) {}
};

void HelloBody(void* params) {
    auto p   = std::unique_ptr<Params>(reinterpret_cast<Params*>(params));
    auto ofx = ofx::OMOSFactory::instance();
    auto tab = std::string((p->thread_id - 1) * 4, ' ');
    for (auto k = 0UL; k < p->num_turns; ++k) {
        auto buf = std::ostringstream{};
        buf << tab << "[hello-" << std::to_string(p->thread_id) << "] " << std::to_string(k + 1) << "\n";
        std::cout << buf.str();
        ofx->delayCurrentThread(p->sleep_secs * 1000);
    }
}

int main() {
    auto N = 10U;
    auto ofx = ofx::OMOSFactory::instance();

    auto t1 = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&HelloBody, reinterpret_cast<void*>(new Params{1, N, 1}))
    };
    auto t2 = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&HelloBody, reinterpret_cast<void*>(new Params{2, N, 2}))
    };
    auto t3 = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&HelloBody, reinterpret_cast<void*>(new Params{3, N, 1}))
    };

    t1->start();
    t2->start();
    t3->start();

    ofx->waitOnThread(t1->getOsHandle(), 0);
    ofx->waitOnThread(t2->getOsHandle(), 0);
    ofx->waitOnThread(t3->getOsHandle(), 0);
}

