#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <functional>
#include <vector>
#include "OMOSFactory.hxx"

struct Params {
    unsigned n;
    ofx::OMOSMessageQueue* mq1;
    ofx::OMOSMessageQueue* mq2;
    Params(unsigned int n, ofx::OMOSMessageQueue* mq1) : n(n), mq1(mq1) {}
    Params(ofx::OMOSMessageQueue* mq1, ofx::OMOSMessageQueue* mq2) : mq1(mq1), mq2(mq2) {}
    explicit Params(ofx::OMOSMessageQueue* mq2) : mq2(mq2) {}
};

struct Message {
    unsigned value;
    explicit Message(unsigned int value) : value(value) {}
};

void log(std::string const& name, std::string const& message) {
    auto buf = std::ostringstream{};
    buf << "[" << name << "] " << message << "\n";
    std::cout << buf.str();
}

void ProducerBodyFn(void* params) {
    auto p = std::unique_ptr<Params>(reinterpret_cast<Params*>(params));
    log("producer", "started");
    for (auto k = 1U; k <= p->n; ++k) {
        p->mq1->put(new Message{k});
    }
    p->mq1->put(new Message{0});
    log("producer", "done");
}

void ConsumerBodyFn(void* params) {
    auto p = std::unique_ptr<Params>(reinterpret_cast<Params*>(params));
    log("consumer", "started");
    do {
        p->mq2->pend();
        auto msg = static_cast<Message*>(p->mq2->get());
        auto value = msg->value;
        delete msg;
        if (value == 0) break;
        log("consumer", std::to_string(value));
    } while (true);
    log("consumer", "done");
}

void TransformerBodyFn(void* params) {
    auto p = std::unique_ptr<Params>(reinterpret_cast<Params*>(params));
    log("transformer", "started");
    do {
        p->mq1->pend();
        auto msg = static_cast<Message*>(p->mq1->get());
        msg->value *= 2;
        log("transformer", std::to_string(msg->value));
        p->mq2->put(msg);
        if (msg->value == 0) break;
    } while (true);
    log("transformer", "done");
}

int main(int argc, char** argv) {
    auto N   = 1'000U;
    auto ofx = ofx::OMOSFactory::instance();

    auto mq1 = std::unique_ptr<ofx::OMOSMessageQueue>{ ofx->createOMOSMessageQueue(false, 8) };
    auto mq2 = std::unique_ptr<ofx::OMOSMessageQueue>{ ofx->createOMOSMessageQueue(false, 8) };

    auto producer = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&ProducerBodyFn, reinterpret_cast<void*>(new Params{N, mq1.get()}))
    };
    auto transformer = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&TransformerBodyFn, reinterpret_cast<void*>(new Params{mq1.get(), mq2.get()}))
    };
    auto consumer = std::unique_ptr<ofx::OMOSThread>{
            ofx->createOMOSThread(&ConsumerBodyFn, reinterpret_cast<void*>(new Params{mq2.get()}))
    };

    consumer->start();
    transformer->start();
    producer->start();

    ofx->waitOnThread(producer->getOsHandle(), 0);
    ofx->waitOnThread(transformer->getOsHandle(), 0);
    ofx->waitOnThread(consumer->getOsHandle(), 0);
}
