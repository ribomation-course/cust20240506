#include "OMOSFactory.hxx"
#include "Tiny_OMOSFactory.hxx"

namespace ofx {
    OMOSFactory* OMOSFactory::instance() {
        if (the_instance == nullptr) {
            the_instance = new ofx::impl::Tiny_OMOSFactory();
        }
        return the_instance;
    }

    OMBoolean OMOSThread::exeOnMyThread() {
        return false;
    }
}

