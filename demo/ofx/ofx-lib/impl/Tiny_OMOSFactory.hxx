#pragma once
#include "OMOSFactory.hxx"
#include "Tiny_OMOSThread.hxx"
#include "Tiny_OMOSMutex.hxx"
#include "Tiny_OMOSMessageQueue.hxx"

namespace ofx::impl {

    struct Tiny_OMOSFactory : ofx::OMOSFactory {
        OMOSThread*
        createOMOSThread(void (* tfunc)(void*), void* param,
                         const char* threadName,
                         long stackSize) override {
            return new Tiny_OMOSThread{tfunc, param};
        }

        OMOSMutex*
        createOMOSMutex() override {
            return new Tiny_OMOSMutex();
        }

        OMOSMessageQueue*
        createOMOSMessageQueue(OMBoolean shouldGrow, const long messageQueueSize) override {
            return new Tiny_OMOSMessageQueue{messageQueueSize, shouldGrow};
        }

        OMBoolean waitOnThread(void* osHandle, timeUnit ms) override;
        void delayCurrentThread(timeUnit ms) override;
    };

}
