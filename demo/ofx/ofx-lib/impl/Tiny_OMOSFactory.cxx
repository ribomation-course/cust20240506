#include <ctime>
#include <cstring>
#include <cerrno>
#include <pthread.h>
#include <unistd.h>
#include "Tiny_OMOSFactory.hxx"

namespace ofx::impl {

    OMBoolean Tiny_OMOSFactory::waitOnThread(void* osHandle, timeUnit ms) {
        if (ms == 0) {
            ms = 10 * 1000;
        }
        struct timespec ts{};
        if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
            throw std::invalid_argument{"clock_gettime() failed: "s + ::strerror(errno)};
        }
        ts.tv_nsec += ms * 1'000'000;

        auto* thrId = reinterpret_cast<pthread_t*>(osHandle);
        auto rc = pthread_timedjoin_np(*thrId, nullptr, &ts);
        return rc == 0;
    }

    void Tiny_OMOSFactory::delayCurrentThread(timeUnit ms) {
        ::usleep(ms * 1000);
    }

}
