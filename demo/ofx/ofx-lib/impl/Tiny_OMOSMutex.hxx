#pragma once
#include <pthread.h>
#include "Tiny_OMOSFactory.hxx"

namespace ofx::impl {

    class Tiny_OMOSMutex : public ofx::OMOSMutex {
        pthread_mutex_t mx{};
    public:
        Tiny_OMOSMutex() {
            pthread_mutex_init(&mx, nullptr);
        }
        ~Tiny_OMOSMutex() override {
            pthread_mutex_destroy(&mx);
        }

        void lock() override {
            pthread_mutex_lock(&mx);
        }
        void unlock() override {
            pthread_mutex_unlock(&mx);
        }

        void free() override {
            unlock();
        }
        void* getOsHandle() const override {
            auto m = const_cast<pthread_mutex_t*>(&mx);
            return reinterpret_cast<void*>(m);
        }
    };

}
