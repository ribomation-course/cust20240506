#pragma once
#include <deque>
#include <pthread.h>
#include "Tiny_OMOSFactory.hxx"

namespace ofx::impl {
    
    struct Guard {
        pthread_mutex_t* mutex;
        Guard(pthread_mutex_t* mx) : mutex{mx} {
            pthread_mutex_lock(mutex);
        }
        ~Guard() {
            pthread_mutex_unlock(mutex);
        }
    };

    class Tiny_OMOSMessageQueue : public ofx::OMOSMessageQueue {
        long max_size{};
        std::deque<unsigned char*> queue{};
        pthread_mutex_t mutex{};
        pthread_cond_t  not_empty{};
        pthread_cond_t  not_full{};
    public:

        Tiny_OMOSMessageQueue(long max_size, bool dynamic) {
            if (dynamic) {
                this->max_size = -1;
            } else {
                this->max_size = max_size;
            }

            pthread_mutexattr_t attrs{};
            pthread_mutexattr_init(&attrs);
            pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE);
            pthread_mutex_init(&mutex, &attrs);
            pthread_mutexattr_destroy(&attrs);

            pthread_cond_init(&not_empty, nullptr);
            pthread_cond_init(&not_full, nullptr);
        }

        ~Tiny_OMOSMessageQueue() override {
            pthread_mutex_destroy(&mutex);
            pthread_cond_destroy(&not_empty);
            pthread_cond_destroy(&not_full);
        }

        OMBoolean put(void* payload, OMBoolean) override {
            auto g = Guard(&mutex);
            if (max_size != -1) {
                while (isFull()) {
                    pthread_cond_wait(&not_full, &mutex);
                }
            }
            queue.push_back(static_cast<unsigned char*>(payload));
            pthread_cond_broadcast(&not_empty);
            return true;
        }

        void* get() override {
            auto g = Guard(&mutex);
            void* payload = nullptr;
            if (not isEmpty()) {
                payload = queue.front();
                queue.pop_front();
            }
            pthread_cond_broadcast(&not_full);
            return payload;
        }

        void pend() override {
            auto g = Guard(&mutex);
            while (isEmpty()) {
                pthread_cond_wait(&not_empty, &mutex);
            }
        }

        int isEmpty() override {
            auto g = Guard(&mutex);
            return queue.empty();
        }

        OMBoolean isFull() override {
            auto g = Guard(&mutex);
            return queue.size() == static_cast <unsigned long>(max_size);
        }

    };

}
