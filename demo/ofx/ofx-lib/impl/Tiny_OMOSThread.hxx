#pragma once
#include <string>
#include <stdexcept>
#include <pthread.h>
#include "Tiny_OMOSFactory.hxx"

namespace ofx::impl {
    using namespace std::string_literals;

    struct NotImplemented : std::runtime_error {
        explicit NotImplemented(std::string const& fn)
                : runtime_error("function not implemented: "s + fn) {}
    };

    class Tiny_OMOSThread : public ofx::OMOSThread {
        ThreadBodyFn bodyFn;
        void*        param;
        pthread_t    thrId{};
        static void* bodyWrapper(void* self) {
            auto thr = reinterpret_cast<Tiny_OMOSThread*>(self);
            (*thr->bodyFn)(thr->param);
            return nullptr;
        }
    public:
        Tiny_OMOSThread(ThreadBodyFn bodyFn, void* param)
            : bodyFn(bodyFn), param(param) {}
        ~Tiny_OMOSThread() override = default;
        void start() override {
            pthread_create(&thrId, nullptr, &bodyWrapper, this);
        }
        void* getOsHandle() const override {
            auto t = const_cast<pthread_t*>(&thrId);
            return reinterpret_cast<void*>(t);
        }

        void getThreadEndClbk(OMOSThreadEndCallBack* clb_p,
                              void** arg1_p,
                              OMBoolean onExecuteThread) override {
            throw NotImplemented{"getThreadEndClbk"};
        }
        void suspend() override {
            throw NotImplemented{"suspend"};
        }
        void resume() override {
            throw NotImplemented{"resume"};
        }
        void setEndOSThreadInDtor(OMBoolean val) override {
            throw NotImplemented{"setEndOSThreadInDtor"};
        }
        void setPriority(int pr) override {
            throw NotImplemented{"setPriority"};
        }
    };

}