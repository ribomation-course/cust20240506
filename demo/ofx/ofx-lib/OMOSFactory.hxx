#pragma once

namespace ofx {
    using OMBoolean = bool;
    using OMOSThreadEndCallBack = void (*)(void*);
    using ThreadBodyFn = void (*)(void*);
    using timeUnit = unsigned;

    struct OMOSThread {
        static constexpr long DefaultStackSize = 10 * 1024;
        static constexpr long DefaultMessageQueueSize = 32;
        virtual ~OMOSThread() = default;
        virtual OMBoolean exeOnMyThread();
        virtual void* getOsHandle() const = 0;
        virtual void getThreadEndClbk(OMOSThreadEndCallBack* clb_p,
                                      void** arg1_p,
                                      OMBoolean onExecuteThread) = 0;
        virtual void resume() = 0;
        virtual void setEndOSThreadInDtor(OMBoolean val) = 0;
        virtual void setPriority(int pr) = 0;
        virtual void start() = 0;
        virtual void suspend() = 0;
    };

    struct OMOSMutex {
        virtual ~OMOSMutex() = default;
        virtual void lock() = 0;
        virtual void unlock() = 0;
        virtual void free() = 0;
        virtual void* getOsHandle() const = 0;
    };

    struct OMOSMessageQueue {
        virtual ~OMOSMessageQueue() = default;
        virtual void* get() = 0;
        virtual void pend() = 0;
        virtual OMBoolean put(void* m, OMBoolean fromISR = false) = 0;
        virtual int isEmpty() = 0;
        virtual OMBoolean isFull() = 0;
    };

    class OMOSFactory {
        static inline OMOSFactory* the_instance = nullptr;
    public:
        static OMOSFactory* instance();

        virtual OMOSThread* createOMOSThread(
                void tfunc(void*),
                void* param,
                const char* threadName = nullptr,
                long stackSize = OMOSThread::DefaultStackSize) = 0;

        virtual OMOSMessageQueue* createOMOSMessageQueue(
                OMBoolean shouldGrow = true,
                const long messageQueueSize = OMOSThread::DefaultMessageQueueSize) = 0;

        virtual OMOSMutex* createOMOSMutex() = 0;

        virtual OMBoolean waitOnThread(void* osHandle, timeUnit ms) = 0;
        virtual void delayCurrentThread(timeUnit ms) = 0;
    };

}
