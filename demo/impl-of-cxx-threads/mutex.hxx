#pragma once
#include <pthread.h>

namespace ribomation::concurrent {

    class Mutex {
        mutable pthread_mutex_t mutex{};
    public:
        Mutex() {
            pthread_mutexattr_t attrs;
            pthread_mutexattr_init(&attrs);
            pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE);
            pthread_mutex_init(&mutex, &attrs);
            pthread_mutexattr_destroy(&attrs);
        }
        ~Mutex() { pthread_mutex_destroy(&mutex); }
        void lock() const { pthread_mutex_lock(&mutex); }
        void unlock() const { pthread_mutex_unlock(&mutex); }
        auto native() const { return &mutex; }
    };

    class LockGuard {
        Mutex& mutex;
    public:
        explicit LockGuard(Mutex& mutex) : mutex(mutex) {
            mutex.lock();
        }
        ~LockGuard() { mutex.unlock(); }
    };

}
