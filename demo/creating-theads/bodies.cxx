#include <iostream>
#include <thread>
using std::cout;

void usecase_lambda() {
    auto body = [](){
        cout << "[thread] Hello from a lambda thread\n";
    };
    auto thr = std::jthread{body};
    cout << "[main] usecase_lambda\n";
}

void bodyFn() {
    cout << "[thread] Hello from a function thread\n";
}
void usecase_function() {
    auto thr = std::jthread{bodyFn};
    cout << "[main] usecase_function\n";
}

struct BodyFunctor {
    void operator()() {
        cout << "[thread] Hello from a functor thread\n";
    }
};
void usecase_functor() {
    auto obj = BodyFunctor{};
    auto thr = std::jthread{obj};
    cout << "[main] usecase_functor\n";
}

struct BodyMethod {
    void body() {
        cout << "[thread] Hello from a method thread\n";
    }
};
void usecase_method() {
    auto obj = BodyMethod{};
    auto thr = std::jthread{&BodyMethod::body, obj};
    cout << "[main] usecase_method\n";
}

int main() {
    cout << "[main] before threads\n";
    usecase_lambda();
    usecase_function();
    usecase_functor();
    usecase_method();
    cout << "[main] after threads.\n";
}

