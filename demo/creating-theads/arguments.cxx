#include <iostream>
#include <string>
#include <thread>
using namespace std::string_literals;
using std::cout;
using std::string;

void bodyFn(int id, string msg) {
    cout << "[thread] id=" << id << ", " << msg << "\n";
}
int main() {
    cout << "[main] before threads\n";
    auto body = [](int id, string msg) {
        cout << "[thread] id=" << id << ", " << msg << "\n";
    };
    {
        auto t1 = std::jthread{body, 1, "Lambda expression"s};
        auto t2 = std::jthread{bodyFn, 2, "Ordinary function"s};
    }
    cout << "[main] after threads\n";
}

