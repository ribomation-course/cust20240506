#include <iostream>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;
namespace ch = std::chrono;
using std::cout;

void usecase_sleep_for() {
    auto body = []() {
        cout << "[thread] before sleep for .75s, id="
             << std::this_thread::get_id() << "\n";
        auto interval = 0.75s;
        std::this_thread::sleep_for(interval);
        cout << "[thread] after sleep\n";
    };
    auto thr = std::jthread{body};
}

void usecase_sleep_until() {
    auto body = []() {
        cout << "[thread] before sleep until .85 seconds from now, id="
             << std::this_thread::get_id() << "\n";
        auto time_point = ch::system_clock::now() + 0.85s;
        std::this_thread::sleep_until(time_point);
        cout << "[thread] after sleep\n";
    };
    auto thr = std::jthread{body};
}

void usecase_yield() {
    auto body = []() {
        cout << "[thread] before sleep for .65 seconds, id="
             << std::this_thread::get_id() << "\n";
        auto start = ch::high_resolution_clock::now();
        auto end = start + 0.65s;
        do {
            std::this_thread::yield();
        } while (ch::high_resolution_clock::now() < end);
        cout << "[thread] after sleep\n";
    };
    auto thr = std::jthread{body};
}

int main() {
    cout << "[main] enter, id="
         << std::this_thread::get_id() << "\n";
    usecase_sleep_for();
    usecase_sleep_until();
    usecase_yield();
    cout << "[main] exit, id="
         << std::this_thread::get_id() << "\n";
}

