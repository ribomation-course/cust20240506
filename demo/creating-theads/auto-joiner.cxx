#include <iostream>
#include <thread>
#include <chrono>
namespace cr = std::chrono;
using namespace std::chrono_literals;
using std::cout;

struct AutoJoiner {
    std::thread& thr;
    explicit AutoJoiner(std::thread& thr) : thr(thr) {}
    ~AutoJoiner() { thr.join(); }
};

int main() {
    cout << "[main] before thread block\n";
    auto start = cr::high_resolution_clock::now();
    {
        auto t = std::thread{[] {
            cout << "[thread] start\n";
            std::this_thread::sleep_for(1.5s);
            cout << "[thread] stop\n";
        }};
        auto j = AutoJoiner{t};
        cout << "[main] last in thread block\n";
    };
    cout << "[main] after thread block\n";
    auto stop = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(stop - start);
    cout << "[main] elapsed " << elapsed.count() << " ms\n";
}

