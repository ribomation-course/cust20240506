#include <iostream>
#include <thread>
#include <syncstream>
using std::cout;

int main() {
    auto t1 = std::jthread{[]() { cout << "[thread] Hello from a C++ thread\n"; }};
    auto t2 = std::jthread{};

    //t2 = t1;
    // error: use of deleted function
    // ‘std::jthread& std::jthread::operator=(const std::jthread&)’

    std::osyncstream{cout}
            << std::boolalpha
            << "[main] t1.joinable=" << t1.joinable()
            << " , t2.joinable=" << t2.joinable() << "\n";
    t2 = std::move(t1);
    std::osyncstream{cout}
            << std::boolalpha
            << "[main] t1.joinable=" << t1.joinable()
            << ", t2.joinable=" << t2.joinable() << "\n";

    try { t1.join(); } catch (std::system_error& x) { cout << "failed t1.join: " << x.what() << "\n"; }
    cout << "[main] before t2.join\n";
    t2.join();
    cout << "[main] exit\n";
}

