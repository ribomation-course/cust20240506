#pragma once
#include <thread>
#include <latch>
#include <memory_resource>
#include "message-queue-bounded.hxx"

namespace ribomation::concurrent {
    using byte = unsigned char;
    namespace m = std::pmr;

    template<typename MessageType, unsigned MaxMessages = 16U, unsigned HeapSize = 100'000U>
    class MessageThread {
        std::jthread                                    runner{};
        MessageQueueBounded<MessageType*, MaxMessages>  inbox{};
        m::synchronized_pool_resource*                  heap = nullptr;
        std::latch                                      fullyStarted{1};
        void thread_body() {
            byte storage[HeapSize]{};
            auto mem = m::monotonic_buffer_resource{std::data(storage), std::size(storage), m::new_delete_resource()};
            auto pool = m::synchronized_pool_resource{&mem};
            heap = &pool;
            fullyStarted.count_down();
            run();
        }
    protected:
        virtual void run() = 0;
        MessageType* recv() { return inbox.get(); }
        void dispose(MessageType* msg) { heap->deallocate(msg, sizeof(MessageType)); }
    public:
        virtual ~MessageThread() = default;
        void start() {
            auto kicker = std::jthread{&MessageThread::thread_body, this};
            runner.swap(kicker);
            fullyStarted.wait();
        }
        template<typename... Args>
        void send(Args... args) {
            auto addr = heap->allocate(sizeof(MessageType));
            auto msg  = new (addr) MessageType{args...};
            inbox.put(msg);
        }
    };
}

