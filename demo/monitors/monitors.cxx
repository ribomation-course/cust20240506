#include <iostream>
#include <syncstream>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>

class monitor {
    std::mutex exclusive;
    std::condition_variable event;
public:
    virtual ~monitor() = default;
protected:
    struct WaitHandler {
        monitor& self;
        std::unique_lock<std::mutex>& lck;
        WaitHandler(monitor& self, std::unique_lock<std::mutex>& lck) : self(self), lck(lck) {}
        void wait_until(std::function<bool()> ready) {
            self.event.wait(lck, std::move(ready));
        }
    };

    struct NotifyHandler {
        monitor& self;
        explicit NotifyHandler(monitor& self) : self(self) {}
        void notify() {
            self.event.notify_all();
        }
    };

    void synchronized(std::function<void(WaitHandler&, NotifyHandler&)> const& stmts) {
        auto guard       = std::unique_lock<std::mutex>{exclusive};
        auto wait_hdlr   = WaitHandler{*this, guard};
        auto notify_hdlr = NotifyHandler{*this};
        stmts(wait_hdlr, notify_hdlr);
    }
};

class Transfer : protected monitor {
    long payload{};
    bool full{};
public:
    void send(decltype(payload) x) {
        synchronized([this, &x](WaitHandler& w, NotifyHandler& n) {
            w.wait_until([this]() { return not full; });
            payload = x;
            full = true;
            n.notify();
        });
    }

    auto recv() -> decltype(payload) {
        auto x = decltype(payload){};
        synchronized([this, &x](WaitHandler& w, NotifyHandler& n) {
            w.wait_until([this]() { return full; });
            x = payload;
            full = false;
            n.notify();
        });
        return x;
    }
};


int main() {
    auto tx = Transfer{};
    {
        auto receiver = std::jthread{[&tx]() {
            decltype(tx.recv()) sum{};
            for (auto msg = tx.recv(); msg != 0; msg = tx.recv()) {
                std::osyncstream{std::cout} << "RECV: " << msg << "\n";
                sum += msg;
            }
            std::osyncstream{std::cout} << "RECV: sum=" << sum << "\n";
        }};
        auto sender = std::jthread{[&tx]() {
            auto N = 1000L;
            for (auto k = 1L; k <= N; ++k) tx.send(k);
            tx.send(0);
            std::osyncstream{std::cout} << "SEND: sum=" << N * (N + 1) / 2 << "\n";
        }};
    }
    std::cout << "[main] done\n";
}

