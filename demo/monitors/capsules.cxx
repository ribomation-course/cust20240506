#include <iostream>
#include <syncstream>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>


class capsule {
    std::mutex exclusive;
    std::condition_variable event;
public:
    virtual ~capsule() = default;
protected:
    void entry(std::function<bool()> granted, std::function<void()> const& body) {
        auto guard = std::unique_lock<std::mutex>{exclusive};
        event.wait(guard, std::move(granted));
        body();
        event.notify_all();
    }
};


class Transfer : protected capsule {
    long payload{};
    bool full{};
public:
    void send(decltype(payload) x) {
        entry([this]() { return not full; }, [this, &x]() {
            payload = x;
            full = true;
        });
    }

    auto recv() -> decltype(payload) {
        auto x = decltype(payload){};
        entry([this]() { return full; }, [this, &x]() {
            x = payload;
            full = false;
        });
        return x;
    }
};


int main() {
    auto tx = Transfer{};
    {
        auto receiver = std::jthread{[&tx]() {
            decltype(tx.recv()) sum{};
            for (auto msg = tx.recv(); msg != 0; msg = tx.recv()) {
                std::osyncstream{std::cout} << "RECV: " << msg << "\n";
                sum += msg;
            }
            std::osyncstream{std::cout} << "RECV: sum=" << sum << "\n";
        }};
        auto sender = std::jthread{[&tx]() {
            auto N = 1000L;
            for (auto k = 1L; k <= N; ++k) tx.send(k);
            tx.send(0);
            std::osyncstream{std::cout} << "SEND: sum=" << N * (N + 1) / 2 << "\n";
        }};
    }
    std::cout << "[main] done\n";
}


