#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <syncstream>


using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;
using std::string;

class StockTicker {
    const string name;
    long value;
    mutable std::shared_mutex rwl{};
public:
    StockTicker(string name, long value)
            : name(std::move(name)), value(value) {}
    string const& symbol() const { return name; }

    auto read() const {
        auto g = std::shared_lock<std::shared_mutex>{rwl};
        return value;
    }
    void increment(long amount) {
        auto g = std::unique_lock<std::shared_mutex>{rwl};
        value += amount;
    }
};

struct Reader {
    const unsigned id;
    StockTicker& stock;
    unsigned long seed;
    Reader(unsigned id, StockTicker& stock, unsigned long seed)
            : id(id), stock(stock), seed(seed) {}

    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto interval = std::uniform_int_distribution<long>{500, 1000};
        auto const tab = string(id * 6, ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] "
                              << stock.symbol() << "=" << stock.read() << " kr\n";
            auto ms = interval(r);
            std::this_thread::sleep_for(cr::milliseconds(ms));
        } while (true);
    }
};

struct Writer {
    StockTicker& stock;
    unsigned long seed;
    Writer(StockTicker& stock, unsigned long seed)
            : stock(stock), seed(seed) {}

    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto amount = std::normal_distribution<>{0, 100};
        auto interval = std::uniform_int_distribution<long>{2000, 5000};
        do {
            std::this_thread::sleep_for(cr::milliseconds(interval(r)));
            auto value = amount(r);
            stock.increment(static_cast<long>(value));
            osyncstream{cout} << "[writer] UPDATED "
                              << stock.symbol() << "=" << value << " kr\n";
        } while (true);
    }
};

int main() {
    auto R = std::random_device{};
    auto stock = StockTicker{"GOOG", 1000};
    auto writer = std::jthread{Writer{stock, R()}};
    auto reader1 = std::jthread{Reader{1U, stock, R()}};
    auto reader2 = std::jthread{Reader{2U, stock, R()}};
    auto reader3 = std::jthread{Reader{3U, stock, R()}};
}
