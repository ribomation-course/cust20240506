#include <iostream>
#include <syncstream>
#include <thread>
#include "receivable.hxx"
#include "rendezvous.hxx"
namespace rm = ribomation::concurrent;
using std::cout;
using std::osyncstream;

using Message = rm::RendezvousMessage<unsigned, unsigned long>;

struct Server : rm::Receivable<Message*> {
    auto compute(unsigned arg) -> unsigned long {
        auto msg = Message{arg};
        send(&msg);
        return msg.promise.get_future().get();
    }

    void run() {
        do {
            auto msg = recv();
            auto n = msg->payload;
            if (n == 0) {
                msg->promise.set_value(0);
                break;
            }
            auto r = n * (n + 1) / 2;
            msg->promise.set_value(r);
        } while (true);
        cout << "[server] done\n";
    }
};

struct Client {
    unsigned id, from, to;
    Server& server;
    Client(unsigned int id, unsigned int from, unsigned int to, Server& server)
    : id(id), from(from), to(to), server(server) {}

    void run() const {
        for (auto k = from; k <= to; ++k) {
            auto result = server.compute(k);
            osyncstream{cout} << "[client-" << id << "] " << k << " -> " << result << "\n";
        }
        osyncstream{cout} << "[client-" << id << "] done\n";
    }
};

int main() {
    auto server = Server{};
    auto serverThread = std::jthread{&Server::run, &server};
    {
        auto client = Client{1, 1, 100, server};
        auto clientThread = std::jthread{&Client::run, &client};
        auto client2 = Client{2, 100, 200, server};
        auto client2Thread = std::jthread{&Client::run, &client2};
        auto client3 = Client{3, 200, 300, server};
        auto client3Thread = std::jthread{&Client::run, &client3};
    }
    server.compute(0);
}

