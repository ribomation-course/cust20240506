#pragma once
#include "message-queue-unbounded.hxx"

namespace ribomation::concurrent {

    template<typename MessageType>
    struct Receivable {
    private:
        MessageQueueUnbounded<MessageType> inbox;

    protected:
        MessageType recv() { return inbox.get(); }

    public:
        void send(MessageType msg) { inbox.put(msg); }

        Receivable() = default;
        virtual ~Receivable() = default;
        Receivable(Receivable const&) = delete;
        auto operator =(Receivable const&) -> Receivable& = delete;
    };

}
