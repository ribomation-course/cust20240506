#include "account.hxx"
#include "atm.hxx"

int main(int argc, char** argv) {
    auto atm = ATM<Account>{};
    atm.args(argc, argv);
    atm.run();
}

