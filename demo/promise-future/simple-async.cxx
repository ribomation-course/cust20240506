#include <iostream>
#include <string>
#include <chrono>
#include <future>
#include <thread>

namespace cr = std::chrono;
using std::cout;
using std::string;
using namespace std::chrono_literals;

int main() {
    auto start = cr::high_resolution_clock::now();
    auto elapsed = [&start](string const& msg) {
        auto end = cr::high_resolution_clock::now();
        auto time = cr::duration_cast<cr::milliseconds>(end - start);
        cout << "[main] " << msg << ": elapsed " << time.count() << " ms\n";
    };

    cout << "[main] enter\n";
    auto result1 = std::async([] {
        std::this_thread::sleep_for(1s); return 21;
    });
    elapsed("after async1");
    auto result2 = std::async([] {
        std::this_thread::sleep_for(2s); return 2;
    });
    elapsed("after async2");
    cout << "[main] waiting results...\n";
    cout << "[main] answer=" << (result1.get() * result2.get()) << "\n";
    elapsed("after answer");
}

