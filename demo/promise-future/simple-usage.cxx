#include <iostream>
#include <chrono>
#include <future>
#include <thread>
namespace cr = std::chrono;
using std::cout;
using namespace std::chrono_literals;

int main() {
    auto start = cr::high_resolution_clock::now();
    {
        auto serverChannel = std::promise<int>{};
        auto clientChannel = serverChannel.get_future();
        {
            auto server = std::jthread{[&serverChannel] {
                cout << "[server] started\n";
                std::this_thread::sleep_for(2s);
                serverChannel.set_value(42);
            }};
        }
        auto result = clientChannel.get();
        cout << "[main] answer to everything = " << result << "\n";
    }
    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(end - start);
    cout << "[main] elapsed time: " << elapsed.count() << " ms\n";
}

