#pragma once

#include <iostream>
#include <string>
#include <syncstream>
#include <memory_resource>
#include <cctype>
#include "message-queue.hxx"
#include "message.hxx"


namespace ribomation::app {
    namespace m = std::pmr;
    namespace co = ribomation::concurrent;

    struct Consumer {
        co::MessageQueue<Message*>* inbox;
        m::synchronized_pool_resource* heap;

        Consumer(co::MessageQueue<Message*>* inbox_, m::synchronized_pool_resource* heap_)
                : inbox{inbox_}, heap{heap_} {}

        void run() {
            std::osyncstream{std::cout} << "Consumer start\n";
            auto charCount = 0UL;
            for (auto msg = inbox->get(); not msg->last; msg = inbox->get()) {
                //std::osyncstream{std::cout} << "Consumer: [" << inbox->count() << "]\n";

                std::osyncstream{std::cout} << msg->getText() << "\n";
                charCount += msg->length;
                heap->deallocate(msg, sizeof(Message));
            }
            std::osyncstream{std::cout} << "Consumer done\n";
            std::osyncstream{std::cout} << "Consumer: num chars=" << charCount << "\n";
        }
    };
}

