#include <iostream>
#include <stdexcept>
#include <thread>

#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <sys/wait.h>

#include "shared-memory.hxx"
#include "message-queue.hxx"
#include "message.hxx"
#include "loader-thread.hxx"
#include "transform-thread.hxx"
#include "consumer-thread.hxx"

namespace shm = ribomation::shared_memory;
namespace con = ribomation::concurrent;
namespace app = ribomation::app;
namespace pmr = std::pmr;
using namespace std::string_literals;
using std::cout;

int main() {
    size_t storageSize = 100 * con::MessageQueue<app::Message*>::capacity() * sizeof(app::Message); // ¯\_(ツ)_/¯
    size_t shmSize     = storageSize
                         + sizeof(pmr::monotonic_buffer_resource)
                         + sizeof(pmr::synchronized_pool_resource)
                         + 2 * sizeof(con::MessageQueue<app::Message*>);
    auto shm     = shm::SharedMemory{shmSize};
    auto storage = shm.allocate(storageSize);
    auto buffer  = new (shm.allocate(sizeof(pmr::monotonic_buffer_resource))) pmr::monotonic_buffer_resource{storage, storageSize, pmr::null_memory_resource()};
    auto pool    = new (shm.allocate(sizeof(pmr::synchronized_pool_resource))) pmr::synchronized_pool_resource{buffer};

    auto mq1 = new (shm.allocate(sizeof(con::MessageQueue<app::Message*>))) con::MessageQueue<app::Message*>{};
    auto mq2 = new (shm.allocate(sizeof(con::MessageQueue<app::Message*>))) con::MessageQueue<app::Message*>{};
    cout << "[main] SHM: " << shmSize << " bytes\n";

    auto pid1 = ::fork();
    if (pid1 < 0) throw std::runtime_error{"fork() failed: "s + ::strerror(errno)};
    if (pid1 == 0) {
        cout << "[FIRST] start\n";
        auto cons = app::Consumer{mq2, pool};
        auto cnsThr = std::jthread{&app::Consumer::run, &cons};

        auto loader = app::Loader{"../hemsoborna.txt", mq1, pool};
        auto ldrThr = std::jthread{&app::Loader::run, &loader};
        ldrThr.join();
        cnsThr.join();
        cout << "[FIRST] exit\n";
        ::exit(0);
    }

    auto pid2 = ::fork();
    if (pid2 < 0) throw std::runtime_error{"fork() failed: "s + ::strerror(errno)};
    if (pid2 == 0) {
        cout << "[SECOND] start\n";
        auto trans = app::Transformer{1, mq1, mq2};
        auto trsThr = std::jthread{&app::Transformer::run, &trans};
        trsThr.join();
        cout << "[SECOND] exit\n";
        ::exit(0);
    }

    cout << "[main] waiting for processes...\n";
    ::waitpid(pid2, nullptr, 0);
    ::waitpid(pid1, nullptr, 0);
    cout << "[main] done\n";
}
