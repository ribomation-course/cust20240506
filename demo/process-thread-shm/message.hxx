#pragma once

#include <array>
#include <chrono>
#include <string>


namespace ribomation::app {
    namespace cr = std::chrono;
    using ClockUnit = decltype(cr::high_resolution_clock::now());

    struct Message {
        long const id{};
        bool const last{};
        unsigned length{};
        ClockUnit const startTime{};
        std::array<char, 200> payload{};

        Message(long id_, std::string const& txt)
                : id{id_}, length{static_cast<unsigned>(txt.size())},
                  startTime{cr::high_resolution_clock::now()} {
            txt.copy(payload.begin(), length);
        }

        Message(long id_)
                : id{id_}, last{true},
                startTime{cr::high_resolution_clock::now()} {}

        auto getText() const -> std::string {
            return std::string{payload.begin(), payload.begin() + length};
        }

        auto elapsed() const {
            auto endTime = cr::high_resolution_clock::now();
            return cr::duration_cast<cr::nanoseconds>(endTime - startTime);
        }
    };

}
