#include "money-box.hxx"
#include "swift.hxx"

int main(int argc, char** argv) {
    auto swift = SWIFT<Moneybox>{};
    swift.args(argc, argv);
    swift.run();
}

