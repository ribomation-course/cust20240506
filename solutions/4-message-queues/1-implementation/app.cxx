#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue.hxx"

int main() {
    auto const N = 1'000;
    auto Q = ribomation::concurrent::MessageQueueBounded<int, 12>{};

    auto consumer = std::jthread{[&Q] {
        for (auto msg = Q.get(); msg > 0; msg = Q.get()) {
            std::cout << "Consumer: " << msg << std::endl;
        }
        Q.get(); // 2nd producer
    }};
    {
        auto producer = [&Q](unsigned start) {
            for (auto k = start; k <= N; k += 2) Q.put(k);
            std::osyncstream{std::cout} << "producer-" << start << " DONE\n";
        };
        auto odd  = std::jthread{producer, 1U};
        auto even = std::jthread{producer, 2U};
    }
    Q.put(-1);
    Q.put(-2);
}
