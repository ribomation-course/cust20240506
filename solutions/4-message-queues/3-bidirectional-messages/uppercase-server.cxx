#include <iostream>
#include <syncstream>
#include <string>
#include <string_view>
#include <cctype>
#include <algorithm>
#include <thread>
#include "file-slurper.hxx"
#include "receivable.hxx"
#include "rendezvous.hxx"


namespace ribomation::concurrent {
    using namespace std::string_literals;
    using std::string;
    using std::cout;
    using std::osyncstream;
    using Message = RendezvousMessage<string, string>;

    auto toUpper(string const& s) {
        auto buf     = std::string_view{s};
        auto start   = buf.find_first_not_of(' ');
        auto end     = buf.find_last_not_of(' ');
        auto content = buf.substr(start, end - start + 1);
        auto result  = std::string{content.begin(), content.end()};
        for (auto& ch: result) ch = static_cast<char> (::toupper(ch));
        return result;
    }

    struct Server : Receivable<Message*> {
        bool done = false;

        auto compute(string const& arg) -> string {
            auto msg = Message{arg};
            put(&msg);
            return msg.promise.get_future().get();
        }

        void run() {
            do {
                auto msg = get();
                auto str = msg->payload;
                if (done && str.empty()) {
                    msg->promise.set_value(""s);
                    break;
                }
                msg->promise.set_value(toUpper(str));
            } while (true);
            std::cout << "[server] done\n";
        }
    };

    struct Client {
        unsigned id;
        string const filename;
        unsigned maxLines;
        Server& server;

        Client(unsigned id, string filename, unsigned maxLines, Server& server)
                : id(id), filename(std::move(filename)), maxLines(maxLines), server(server) {}

        void run() const {
            osyncstream{cout} << "[client-" << id << "] started\n";

            decltype(maxLines) lineno{};
            for (auto line: io::FileSlurper{filename, maxLines}) {
                ++lineno;
                if (line.empty()) continue;
                osyncstream{cout} << "[client-" << id << "] (" << lineno << ") \""
                                  << line << "\" --> \"" << server.compute(line) << "\"\n";
            }
            osyncstream{cout} << "[client-" << id << "] done\n";
        }
    };
}


int main(int argc, char* argv[]) {
    namespace co = ribomation::concurrent;
    using namespace std::string_literals;
    using std::cout;

    auto max_lines = 200U;
    auto filename1 = "./files/musketeers.txt"s;
    auto filename2 = "./files/shakespeare.txt"s;

    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-n"s) {
            max_lines = std::stoi(argv[++k]);
        } else if (arg == "-f1"s) {
            filename1 = argv[++k];
        } else if (arg == "-f2"s) {
            filename2 = argv[++k];
        } else {
            cout << "Usage: " << argv[0] << " [-n N] [-f1 FILE1] [-f2 FILE2]\n";
            return 1;
        }
    }

    auto server = co::Server{};
    auto serverThr = std::jthread{&co::Server::run, &server};

    {
        auto client1 = co::Client{1, filename1, max_lines, server};
        auto client1Thr = std::jthread{&co::Client::run, &client1};

        auto client2 = co::Client{2, filename2, max_lines, server};
        auto client2Thr = std::jthread{&co::Client::run, &client2};
    }

    cout << "[main] before done\n";
    server.done = true;
    server.compute(""s);
    cout << "[main] done\n";
}
