#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <string_view>
#include <vector>
#include <numeric>
#include <future>

namespace fs = std::filesystem;
using namespace std::literals;

int main(int argc, char* argv[]) {
    auto num_tasks = 4U;
    auto filename  = "./hemsoborna.txt"s;

    for (auto k=1; k<argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-n"s) {
            num_tasks = std::stoi(argv[++k]);
        } else if (arg == "-f"s) {
            filename = argv[++k];
        } else {
            std::cerr << "usage: " << argv[0] << " [-n <tasks>] [-f <filename>]";
            return 1;
        }
    }

    auto size = fs::file_size(filename);
    char buf[size];
    {
        auto file = std::ifstream{filename};
        file.read(buf, static_cast<long>(size));
    }
    auto payload = std::string_view{buf, size};
    std::cout << "loaded " << payload.size() << " bytes from file '" << filename << "'\n";

    auto const chunk_size = payload.size() / num_tasks;
    std::cout << "chunk size is " << chunk_size << " bytes\n";

    auto results = std::vector<std::future<unsigned >>{};
    results.reserve(num_tasks);
    for (auto k = 0U; k < num_tasks; ++k) {
        auto chunk = payload.substr(k * chunk_size, chunk_size);

        auto result = std::async([chunk]() {
            auto cnt = 0U;
            for (auto start = 0U; start < chunk.size(); ++start) {
                auto end = chunk.find(' ', start);
                if (end == std::string_view::npos) break;
                ++cnt;
                start = end;
            }
            return cnt;
        });
        std::cout << "created chunk task #" << (k + 1) << "\n";

        results.push_back(std::move(result));
    }

    auto total = std::accumulate(std::begin(results), std::end(results), 0U,
                                 [](auto sum, auto& f) { return sum + f.get(); });
    std::cout << "----\nNumber of blank chars are " << total << "\n";
}
