#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <syncstream>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;
using std::string;

class Ticker {
    long value{};
    mutable std::shared_mutex rwl{};
public:
    auto read() const {
        auto g = std::shared_lock<std::shared_mutex>{rwl};
        return value;
    }

    auto increment() {
        auto g = std::unique_lock<std::shared_mutex>{rwl};
        return ++value;
    }

    auto decrement() {
        auto g = std::unique_lock<std::shared_mutex>{rwl};
        return --value;
    }
};

class Reader {
    const unsigned id;
    Ticker& ticker;
    unsigned long seed;
public:
    Reader(const unsigned int id, Ticker& ticker, unsigned long seed)
        : id(id), ticker(ticker), seed(seed) {}

    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto delay = std::uniform_int_distribution<long>{400, 1000};
        auto const tab = string(id * 4, ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] " << ticker.read() << "\n";
            std::this_thread::sleep_for(cr::milliseconds(delay(r)));
        } while (true);
    }
};

class Incrementer {
    Ticker& ticker;
    unsigned long seed;
public:
    Incrementer(Ticker& ticker, unsigned long seed) : ticker(ticker), seed(seed) {}
    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto delay = std::uniform_int_distribution<long>{2, 6};
        do {
            std::this_thread::sleep_for(cr::seconds(delay(r)));
            osyncstream{cout} << "[INC] " << ticker.increment() << " ------------\n";
        } while (true);
    }
};

class Decrementer {
    Ticker& ticker;
    unsigned long seed;
public:
    explicit Decrementer(Ticker& ticker, unsigned long seed) : ticker(ticker), seed(seed) {}
    [[noreturn]] void operator ()() {
        auto r = std::default_random_engine{seed};
        auto delay = std::uniform_int_distribution<long>{3, 8};
        do {
            std::this_thread::sleep_for(cr::seconds(delay(r)));
            osyncstream{cout} << "[DEC] " << ticker.decrement() << " ------------\n";
        } while (true);
    }
};

int main() {
    auto R = std::random_device{};
    auto ticker = Ticker{};

    auto incThr = std::jthread{Incrementer{ticker, R()}};
    auto decThr = std::jthread{Decrementer{ticker, R()}};

    auto reader1 = std::jthread{Reader{1, ticker, R()}};
    auto reader2 = std::jthread{Reader{2, ticker, R()}};
    auto reader3 = std::jthread{Reader{3, ticker, R()}};
    auto reader4 = std::jthread{Reader{4, ticker, R()}};
    auto reader5 = std::jthread{Reader{5, ticker, R()}};
    auto reader6 = std::jthread{Reader{6, ticker, R()}};
}

