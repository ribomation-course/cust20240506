#include <iostream>
#include <string>
#include <vector>
#include <syncstream>
#include <thread>
#include <mutex>
#include <condition_variable>

class Chopstick {
    std::mutex exclusive{};
    std::condition_variable not_busy{};
    bool busy = false;
public:
    void acquire() {
        auto g = std::unique_lock{exclusive};
        not_busy.wait(g, [this] { return !busy; });
        busy = true;
    }

    void release() {
        {
            auto g = std::lock_guard{exclusive};
            busy = false;
        }
        not_busy.notify_all();
    }
};

class Philosopher {
    unsigned const id;
    Chopstick& right;
    Chopstick& left;
public:
    Philosopher(unsigned id_, Chopstick& right_, Chopstick& left_)
            : id{id_}, right{right_}, left{left_} {}

    [[noreturn]] void operator()() {
        using namespace std::chrono_literals;
        using std::cout;
        using std::osyncstream;
        auto const tab = std::string(15 * (id - 1), ' ');
        do {
            osyncstream{cout} << tab << "[" << id << "] Request RIGHT\n";
            right.acquire();
            osyncstream{cout} << tab << "[" << id << "] Acquired RIGHT\n";

            osyncstream{cout} << tab << "[" << id << "] Request LEFT\n";
            left.acquire();
            osyncstream{cout} << tab << "[" << id << "] Acquired LEFT\n";

            osyncstream{cout} << tab << "[" << id << "] EATING\n";
            std::this_thread::sleep_for(50ms);
            osyncstream{cout} << tab << "[" << id << "] EATING DONE\n";

            left.release();
            osyncstream{cout} << tab << "[" << id << "] Released LEFT\n";
            right.release();
            osyncstream{cout} << tab << "[" << id << "] Released RIGHT\n";
        } while (true);
    }
};

int main(int argc, char** argv) {
    using namespace std::string_literals;
    auto P = 5U;
    auto no_deadlock = false;

    for (auto k=1; k<argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "--philosopher"s) {
            P = std::stoi(argv[++k]);
        } else if (arg == "--no-deadlock"s) {
            no_deadlock = true;
        }
    }

    auto chopsticks = std::vector<Chopstick>(P);
    auto philosophers = std::vector<std::jthread>{};
    philosophers.reserve(P);

    for (auto k = 0U; k < P; ++k) {
        auto rightIdx = k;
        auto leftIdx = (k + 1) % P;

        //This solution breaks Coffman's condition: Circular wait
        if (no_deadlock && k == 0) std::swap(rightIdx, leftIdx);

        philosophers.emplace_back(Philosopher{k + 1, chopsticks[rightIdx], chopsticks[leftIdx]});
    }
}
