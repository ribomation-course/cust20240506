#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <syncstream>

class Counter {
    mutable std::recursive_mutex exclusive;
    long value = 0;
public:
    void increment() {
        auto guard = std::lock_guard<std::recursive_mutex>{exclusive};
        ++value;
    }

    void decrement() {
        auto guard = std::lock_guard<std::recursive_mutex>{exclusive};
        --value;
    }

    long get() const {
        auto guard = std::lock_guard<std::recursive_mutex>{exclusive};
        return value;
    }
};

void operator ++(Counter& c) { c.increment(); }
void operator --(Counter& c) { c.decrement(); }
auto operator *(Counter& c) { return c.get(); }

int main() {
    auto counter = Counter{};

    auto updater = [&counter](unsigned id, unsigned N) {
        std::osyncstream{std::cout} << "[updater-" << id << "] START: cnt=" << *counter /*counter.get()*/ << "\n";

        for (auto i = 0U; i < N; ++i) ++counter; //counter.increment();
        std::osyncstream{std::cout} << "[updater-" << id << "] MIDDLE: cnt=" << *counter /*counter.get()*/ << "\n";

        for (auto i = 0U; i < N; ++i) --counter; //counter.decrement();
        std::osyncstream{std::cout} << "[updater-" << id << "] END: cnt=" << *counter /*counter.get()*/ << "\n";
    };

    {
        auto const N = 10'000;
        auto const T = std::thread::hardware_concurrency();
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);

        for (auto id = 1U; id <= T; ++id) threads.emplace_back(updater, id, N);
    }
    std::cout << "[main] counter=" << *counter /*counter.get()*/ << "\n";
}
