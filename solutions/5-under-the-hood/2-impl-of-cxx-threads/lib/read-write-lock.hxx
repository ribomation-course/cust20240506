#pragma once
#include <pthread.h>

namespace ribomation::concurrent {

    class ReadWriteLock {
        mutable pthread_rwlock_t rwlock{};
    public:
        ReadWriteLock() {
            pthread_rwlock_init(&rwlock, nullptr);
        }
        ~ReadWriteLock() {
            pthread_rwlock_destroy(&rwlock);
        }

        void readLock()  const { pthread_rwlock_rdlock(&rwlock); }
        void writeLock() const { pthread_rwlock_wrlock(&rwlock); }
        void unlock()    const { pthread_rwlock_unlock(&rwlock); }
        auto native()    const { return &rwlock; }
    };

    class LockForReading {
        ReadWriteLock& rwlock;
    public:
        explicit LockForReading(ReadWriteLock& rwlock) : rwlock(rwlock) {
            rwlock.readLock();
        }
        ~LockForReading() { rwlock.unlock(); }
    };

    class LockForWriting {
        ReadWriteLock& rwlock;
    public:
        explicit LockForWriting(ReadWriteLock& rwlock) : rwlock(rwlock) {
            rwlock.writeLock();
        }
        ~LockForWriting() { rwlock.unlock(); }
    };
}

