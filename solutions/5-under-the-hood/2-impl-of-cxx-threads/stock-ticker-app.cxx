#include <iostream>
#include <string>
#include <random>
#include <unistd.h>
#include "thread.hxx"
#include "syncstream.hxx"
#include "read-write-lock.hxx"

namespace co = ribomation::concurrent;
using namespace std::literals;

class Ticker {
    long value{};
    mutable co::ReadWriteLock rwl{};
public:
    auto read() const {
        auto g = co::LockForReading{rwl};
        return value;
    }

    auto update(int amount) {
        auto g = co::LockForWriting{rwl};
        value += amount;
        return value;
    }
};

int main() {
    auto ticker = Ticker{};

    auto updater = co::Thread{[&ticker] {
        auto R = std::random_device{};
        auto amount = std::uniform_int_distribution<int>{-50, +50};
        auto delay = std::uniform_int_distribution<int>{1, 5};
        for (;;) {
            sleep(delay(R));
            co::osyncstream{std::cout} << "[writer] --- " << ticker.update(amount(R)) << " ----------------\n";
        }
    }};

    auto reader = [&ticker](unsigned id) {
        auto R = std::default_random_engine{id * 4711UL};
        auto tab = std::string(id * 5, ' ');
        auto delay = std::uniform_int_distribution<int>{100, 1000};
        for (;;) {
            co::osyncstream{std::cout} << tab << "[reader-" << id << "] " << ticker.read() << "\n";
            usleep(delay(R) * 1000);
        }
    };

    auto r1 = co::Thread{reader, 1U};
    auto r2 = co::Thread{reader, 2U};
    auto r3 = co::Thread{reader, 3U};
}
