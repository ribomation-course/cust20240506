#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* body(void* args) {
    unsigned id = (unsigned) args;
    printf("Hello from thread %u\n", id);
    sleep(1);
    printf("Goodbye from thread %u\n", id);
    return NULL;
}

int main() {
    unsigned T = 5;
    pthread_t threads[T];
    for (unsigned long k = 0; k < T; ++k) {
        pthread_create(&threads[k], NULL, body, (void*) (k + 1));
    }
    printf("[main] waiting for threads to finish\n");
    for (unsigned k = 0; k < T; ++k) {
        pthread_join(threads[k], NULL);
    }
    printf("[main] done\n");
    return 0;
}
