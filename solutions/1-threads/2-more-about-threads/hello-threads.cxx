#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

auto bodyLambda = [](unsigned id, unsigned N) {
    auto const tab = string((id - 1) * 5, ' ');
    for (auto k = 1U; k <= N; ++k) {
        osyncstream{cout} << tab << "[Lambda-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(10ms);
    }
};

auto bodyFunc(unsigned id, unsigned N) {
    auto const tab = string((id - 1) * 5, ' ');
    for (auto k = 1U; k <= N; ++k) {
        osyncstream{cout} << tab << "[Func-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(15ms);
    }
}

int main() {
    cout << "[main] enter\n";
    {
        auto const N = 100U;
        auto const T = std::thread::hardware_concurrency();
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) {
            if (id % 2 == 0) {
                threads.emplace_back(bodyLambda, id, N);
            } else {
                threads.emplace_back(bodyFunc, id, N);
            }
        }
    }
    cout << "[main] exit\n";
}
