#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

int main() {
    {
        auto body = [](std::stop_token stop, unsigned id) {
            auto const tab = string((id - 1) * 5, ' ');
            for (auto k = 1U; true; ++k) {
                osyncstream{cout} << tab << "[Lambda-" << id << "] " << k << "\n";
                if (stop.stop_requested()) {
                    osyncstream{cout} << tab << "[Lambda-" << id << "] DONE\n";
                    return;
                }
                std::this_thread::sleep_for(5ns);
            }
        };

        auto const T = std::thread::hardware_concurrency();
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) threads.emplace_back(body, id);

        cout << "[main] before wait\n";
        std::this_thread::sleep_for(2s);
        cout << "[main] after wait\n";
        for (auto&& t : threads) t.request_stop();
        cout << "[main] after request stop\n";
    }
    cout << "[main] exit\n";
}
