#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

auto bodyLambda = [](unsigned id, unsigned N) {
    auto const tab = string((id - 1) * 5, ' ');
    for (auto k = 1U; k <= N; ++k) {
        osyncstream{cout} << tab << "[Lambda-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(50ms);
    }
};

auto bodyFunc(unsigned id, unsigned N) {
    auto const tab = string((id - 1) * 5, ' ');
    for (auto k = 1U; k <= N; ++k) {
        osyncstream{cout} << tab << "[Function-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(55ms);
    }
}

struct Functor {
    const unsigned id;
    const unsigned N;
    Functor(const unsigned int id, const unsigned int n) : id(id), N(n) {}
    void operator()() {
        auto const tab = string((id - 1) * 5, ' ');
        for (auto k = 1U; k <= N; ++k) {
            osyncstream{cout} << tab << "[Object-" << id << "] " << k << "\n";
            std::this_thread::sleep_for(60ms);
        }
    }
};

int main() {
    cout << "[main] enter\n";
    {
        auto const T = std::thread::hardware_concurrency();
        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);

        for (auto id = 1U; id <= T; ++id) {
            auto rest = id % 3;
            if (rest == 0) {
                threads.emplace_back(bodyLambda, id, 100);
            } else if (rest == 1) {
                threads.emplace_back(&bodyFunc, id, 100);
            } else {
                threads.emplace_back(Functor{id, 100});
            }
        }
    }
    cout << "[main] exit\n";
}
