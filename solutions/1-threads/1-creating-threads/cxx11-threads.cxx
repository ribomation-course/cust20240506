#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::string;
using std::cout;
using std::osyncstream;

struct AutoJoiner {
    std::thread& thr;
    explicit AutoJoiner(std::thread& thr) : thr(thr) {}
    ~AutoJoiner() {
        if (thr.joinable()) thr.join();
    }
};

auto body = [](unsigned id, unsigned N) {
    auto const tab = string((id - 1) * 5, ' ');
    for (auto k = 1U; k <= N; ++k) {
        osyncstream{cout} << tab << "[Lambda-" << id << "] " << k << "\n";
        std::this_thread::sleep_for(50ms);
    }
};

int main() {
    cout << "[main] enter\n";
    {
        auto t1 = std::thread{body, 1, 100};
        auto j1 = AutoJoiner{t1};

        auto t2 = std::thread{body, 2, 100};
        auto j2 = AutoJoiner{t2};

        auto t3 = std::thread{body, 3, 100};
        auto j3 = AutoJoiner{t3};

//        This does not really work as anticipated:
//        ----------------------------------------
//        auto const T = std::thread::hardware_concurrency();
//        auto threads = std::vector<AutoJoiner>{};
//        threads.reserve(T);
//        for (auto id = 1U; id <= T; ++id) {
//            auto t = std::thread{body, id, 100};
//            auto j = AutoJoiner{t};
//            threads.emplace_back(j);
//        }
    }
    cout << "[main] exit\n";
}
